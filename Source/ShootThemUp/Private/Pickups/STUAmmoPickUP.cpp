// Shoot Them Up Game, All Rights Reserved.

#include "Pickups/STUAmmoPickUP.h"
#include "Components/STUHealthComponent.h"
#include "Components/STUWeaponComponent.h"
#include "STUUtils.h"

DEFINE_LOG_CATEGORY_STATIC(LogAmmoPickup, All, All);

bool ASTUAmmoPickUP::GivePickUpTo(APawn* PlayerPawn)
{
    const auto HealthComponent = STUUtils::GetSTUPlayerComponent<USTUHealthComponent>(PlayerPawn);
    if (!HealthComponent || HealthComponent->IsDead())
        return false;

    const auto WeaponComponent = STUUtils::GetSTUPlayerComponent<USTUWeaponComponent>(PlayerPawn);
    if (!WeaponComponent)
        return false;

    UE_LOG(LogAmmoPickup, Display, TEXT("Ammo was taken"));
    return WeaponComponent->TryToAddAmmo(WeaponType, ClipsAmount);
    //return true;
}